import React, { Component } from 'react';
import PropTypes from 'prop-types';

import '../styles/header.less'
class Header extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="title">
                <div className="logo">
                    SellVehic
                </div>
                <div className="moto">
                    Best online plateform to sell your vehicles
                </div>
            </div>
        );
    }
}

Header.propTypes = {

};

export default Header;