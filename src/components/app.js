import React, {Component} from 'react';

import Header from './header'
import SellerForm from "./sellerform"
import AdList from "./adlist"

import '../styles/master.less'

export default class App extends Component {
  render(){
    return (
      <div>
        <Header />
        <AdList />
      </div>
    )
  }
}
