import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Input from './input';

import '../styles/button.less'

class SellerForm extends Component {
    constructor(props) {
        super(props);

    }
    render() {
        return (
            <div className="content">
                <div className="box">
                    <Input
                    fieldName="Seller Name"
                    size={50}
                    placeholder="Odin"
                    />
                    <Input
                    fieldName="Address"
                    size={50}
                    placeholder="111, Main Street"
                    />
                    <Input
                    fieldName="City"
                    size={50}
                    placeholder="Asgard"
                    />
                    <Input
                    fieldName="Phone number"
                    size={50}
                    placeholder="XXX-XXX-XXXX, or (XXX)XXX-XXXX"
                    />
                    <Input
                    fieldName="Email"
                    size={50}
                    placeholder="odin@asgard.god"
                    />
                    <Input
                    fieldName="Manufacturer"
                    size={50}
                    placeholder="Maruti Suzuki"
                    />
                    <Input
                    fieldName="Model"
                    size={50}
                    placeholder="Swift Desire"
                    />
                    <Input
                    fieldName="Year"
                    size={50}
                    placeholder="2018"
                    />
                    <button className="button-save">
                        SAVE
                    </button>  
                </div>
            </div>
        );
    }
}

export default SellerForm;