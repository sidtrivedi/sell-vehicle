import React, { Component } from 'react';
import PropTypes from 'prop-types';

import '../styles/adlist.less'
class AdList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var ads = [{
            sellerName: "Odin",
            address: "111, Main Street",
            city: "Asgard",
            phoneNumber: "111-123-7896",
            email: "odin@asgard.god",
            manufacturer: "Tesla",
            model: "S3",
            year: "2018"
        }]
        return (
            <div>
                <div  className="wrapper">
                    <input className="search" size="50" placeholder="Search"/>                
                </div>
                <div className="info-container">
                {
                    ads.map(element=>{
                        return <div className="seller-info" key={`${element.manufacturer}-${element.model}-${element.year}`}>
                        <div className="seller-name"> 
                            {element.sellerName}
                        </div>
                        <div className="attribute"> {element.address}</div>
                        <div className="attribute"> {element.city}</div>
                        <div className="attribute"> {element.phoneNumber}</div>
                        <div className="attribute"> {element.email}</div>
                        <div className="attribute"> {element.manufacturer}</div>
                        <div className="attribute"> {element.model}</div>
                        <div className="attribute"> {element.year}</div>
                        <a target="new_window" href={`http://www.jdpower.com/cars/${element.manufacturer}/${element.model}/${element.year}`} className="attribute">Visit Link</a>
                    </div>
                    })
                }
                </div>
            </div>
        );
    }
}

AdList.propTypes = {

};

export default AdList;